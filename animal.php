<!-- parent -->
<?php 
    class Animal{
        // deklarasi tipe class
        public $name;
        public $legs = 4;
        public $cold_blooded = "no";

        // pada saat dipanggil, ini akan pertama kali run
        public function __construct($name) {
            $this -> name = $name;
        }
    }
?>